package com.location.getlocation.service;

import com.location.getlocation.repository.LocationEntity;
import com.location.getlocation.repository.LocationRequest;
import com.location.getlocation.repository.model.UserDetailTrack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class LocationService {

    public LocationEntity insertLocation(String userId,LocationRequest req) {
        LocationEntity entity = new LocationEntity();
        entity.setUserId(userId);
        entity.setLatitude(req.getLatitude());
        entity.setLongitude(req.getLongitude());
        entity.setPeriod(req.getPeriod());

        return entity;
    }
}
