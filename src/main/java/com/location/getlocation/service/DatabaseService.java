package com.location.getlocation.service;

import com.location.getlocation.repository.LocationEntity;
import com.location.getlocation.repository.model.UserDetailTrack;
import com.location.getlocation.repository.model.UserDetailTrackMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class DatabaseService {
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private void createTable(String tableName){
        String sqlCreate = "CREATE TABLE IF NOT EXISTS " + tableName
            + "  (id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,"
            + "   userId    VARCHAR(256),"
            + "   latitude  DOUBLE,"
            + "   longitude DOUBLE,"
            + "   period    INTEGER,"
            + "   createAt BIGINT)";
//            + "   createAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP)";
        namedParameterJdbcTemplate.getJdbcTemplate().execute(sqlCreate);
    }
    private void createLastLocationTable(){
        String sqlCreate = "CREATE TABLE IF NOT EXISTS location_last"
                + "  (id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,"
                + "   userId    VARCHAR(256) UNIQUE,"
                + "   latitude  DOUBLE,"
                + "   longitude DOUBLE,"
                + "   period    INTEGER,"
                + "   createAt BIGINT)";
        namedParameterJdbcTemplate.getJdbcTemplate().execute(sqlCreate);
    }

    public void save(LocationEntity entity){
        String tableName = generateTableName();
        long millis = System.currentTimeMillis();
        createTable(tableName);
        createLastLocationTable();
        namedParameterJdbcTemplate.getJdbcTemplate().update("INSERT INTO " + tableName + " (userId,latitude,longitude,period,createAt) VALUES (?,?,?,?,?)", new Object[]{entity.getUserId() , entity.getLatitude() , entity.getLongitude() , entity.getPeriod(), millis});
        namedParameterJdbcTemplate.getJdbcTemplate().update("INSERT INTO location_last (userId,latitude,longitude,period,createAt) VALUES (?,?,?,?,?) " +
                                                                "ON DUPLICATE KEY UPDATE userId=?, latitude=?, longitude=?, period=?, createAt=? ",
                                                                new Object[]{entity.getUserId() , entity.getLatitude() , entity.getLongitude() , entity.getPeriod(), millis,
                                                                        entity.getUserId() , entity.getLatitude() , entity.getLongitude() , entity.getPeriod(), millis});
    }

    private String generateTableName(){
        SimpleDateFormat formatter= new SimpleDateFormat("yyyyMMdd");
        Date date = new Date(System.currentTimeMillis());
        return "locations_" +  formatter.format(date);
    }

    private List<String> generateRangeTableName(String startDate,String endDate) throws SQLException {
        LocalDate start = LocalDate.parse(startDate);
        LocalDate end = LocalDate.parse(endDate);

        long numOfDaysBetween = ChronoUnit.DAYS.between(start, end)+1;
        List<LocalDate> dates = IntStream.iterate(0, i -> i + 1).limit(numOfDaysBetween).mapToObj(i -> start.plusDays(i)).collect(Collectors.toList());
        
        List<String> tableList = new ArrayList<>();
        for (LocalDate date : dates) {
            tableList.add("locations_" + date.format(DateTimeFormatter.ofPattern("yyyyMMdd")));
        }
        return checkAvailableTable(tableList);
    }

    private List<String> checkAvailableTable(List<String> tableList) throws SQLException {
        Connection conn = namedParameterJdbcTemplate.getJdbcTemplate().getDataSource().getConnection();
        DatabaseMetaData dbm = conn.getMetaData();

        List<String> availableTable = new ArrayList<>();

        for (String table : tableList){
            ResultSet tables = dbm.getTables(null,null,table,null);
            if (tables.next()) {
                availableTable.add(table);
            }
        }
        return availableTable;
    }

    private String generateQuery(List<String> availableTable){
        List<String> tempQuery = new ArrayList<>();
        for (String tableName : availableTable){
            tempQuery.add("SELECT * FROM "+ tableName +" WHERE userId IN (:ids)");
        }
        return String.join(" UNION ALL ", tempQuery);
    }
    
    public List<UserDetailTrack> getUserDetailLocation(String startDate, String endDate, List<String> userid) throws SQLException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());

        if (startDate == null || startDate.equals("")){
            startDate = formatter.format(date);
        }
        if (endDate == null || endDate.equals("")){
            endDate = formatter.format(date);
        }
        String sqlQuery = this.generateQuery(this.generateRangeTableName(startDate,endDate));

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("ids", userid);
        parameters.addValue("start", startDate);
        parameters.addValue("end", endDate);
        return namedParameterJdbcTemplate.query(sqlQuery, parameters, new UserDetailTrackMapper());
    }

    public List<UserDetailTrack> getUserDetailLastLocation(List<String> userid) {
        String sqlQuery = "SELECT * FROM location_last WHERE userId IN (:ids)";

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("ids", userid);
        return namedParameterJdbcTemplate.query(sqlQuery, parameters, new UserDetailTrackMapper());
    }
}
