package com.location.getlocation.repository;

import lombok.Data;

@Data
public class LocationRequest {
    private double latitude;
    private double longitude;
    private int period;
    private Long exp;
}
