package com.location.getlocation.repository.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LocationResponse {
    boolean isSuccess;
    String message;
}
