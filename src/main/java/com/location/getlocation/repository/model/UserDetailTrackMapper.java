package com.location.getlocation.repository.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDetailTrackMapper implements RowMapper<UserDetailTrack> {

    @Override
    public UserDetailTrack mapRow(ResultSet rs, int rowNum) throws SQLException {
        UserDetailTrack userDetailTrack = new UserDetailTrack();
        userDetailTrack.setUserId(rs.getString("userId"));
        userDetailTrack.setLat(rs.getDouble("latitude"));
        userDetailTrack.setLon(rs.getDouble("longitude"));
        userDetailTrack.setTimestamp(rs.getLong("createAt"));
        return userDetailTrack;
    }
}
