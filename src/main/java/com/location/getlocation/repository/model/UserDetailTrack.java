package com.location.getlocation.repository.model;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class UserDetailTrack {
    private String userId;
    private double lat;
    private double lon;
    private long timestamp;

}
