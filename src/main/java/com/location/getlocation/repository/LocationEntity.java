package com.location.getlocation.repository;

import lombok.Data;
import java.sql.Timestamp;

@Data
public class LocationEntity {

    private String userId;
    private double latitude;
    private double longitude;
    private Timestamp createAt = new Timestamp(System.currentTimeMillis());
    private int period;
}
