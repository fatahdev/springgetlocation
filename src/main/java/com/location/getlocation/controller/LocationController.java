package com.location.getlocation.controller;

import com.location.getlocation.repository.LocationEntity;
import com.location.getlocation.repository.LocationRequest;
import com.location.getlocation.service.DatabaseService;
import com.location.getlocation.service.LocationService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class LocationController {
    @Autowired LocationService locationService;
    @Autowired DatabaseService databaseService;

    private Logger logger = LoggerFactory.getLogger(LocationController.class);

    @PostMapping("/locations")
    public ResponseEntity<?> saveLocation(
        @RequestHeader("User-ID") String userId,
        @RequestBody LocationRequest req
    ){
        try {
            if (req.getExp() < System.currentTimeMillis()) {
                return ResponseEntity.status(HttpStatus.OK).build();
            }
            LocationEntity entity = locationService.insertLocation(userId, req);
            databaseService.save(entity);

            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (Exception e) {
            logger.error("Error saving "+userId+"-"+req, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/locations")
    public ResponseEntity<?> getLocation(
        @RequestParam(value = "start", required = false) String startDate,
        @RequestParam(value = "end", required = false) String endDate,
        @RequestParam(value = "param") List<String> userid
    ) {
        try {
            return ResponseEntity.ok(databaseService.getUserDetailLocation(startDate,endDate, userid));
        } catch (Exception e) {
            logger.error("Error saving "+startDate+"-"+endDate, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
    @GetMapping("/locations/last")
    public ResponseEntity<?> getLocationLast(
        @RequestParam(value = "param") List<String> userid
    ) {
        try {
            return ResponseEntity.ok(databaseService.getUserDetailLastLocation(userid));
        } catch (Exception e) {
            logger.error("Error saving "+userid, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
