package com.location.getlocation;

import com.location.getlocation.controller.LocationController;
import com.location.getlocation.service.LocationService;
import com.location.getlocation.utilities.TomcatWebServerAccessLogCustomizer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@PropertySource(value = "classpath:application.properties")
@ComponentScan(basePackageClasses = {
		TomcatWebServerAccessLogCustomizer.class,
		LocationService.class,
		LocationController.class
})
@SpringBootApplication
public class GetlocationApplication {

	public static void main(String[] args) {
		SpringApplication.run(GetlocationApplication.class, args);
	}

}
