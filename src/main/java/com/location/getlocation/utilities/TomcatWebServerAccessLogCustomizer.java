package com.location.getlocation.utilities;

import org.apache.catalina.valves.AccessLogValve;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.stereotype.Component;

@Component
public class TomcatWebServerAccessLogCustomizer
        implements WebServerFactoryCustomizer<TomcatServletWebServerFactory> {

    @Value("${server.tomcat.accesslog.max-days}")
    int accessLogMaxDays;

    @Autowired
    ServerProperties serverProperties;

    @Override
    public void customize(TomcatServletWebServerFactory factory) {
        ServerProperties.Tomcat tomcatProperties = serverProperties.getTomcat();

        AccessLogValve valve = new AccessLogValve();
        valve.setPattern(tomcatProperties.getAccesslog().getPattern());
        valve.setDirectory(tomcatProperties.getAccesslog().getDirectory());
        valve.setPrefix(tomcatProperties.getAccesslog().getPrefix());
        valve.setSuffix(tomcatProperties.getAccesslog().getSuffix());
        valve.setRenameOnRotate(tomcatProperties.getAccesslog().isRenameOnRotate());
        valve.setFileDateFormat(tomcatProperties.getAccesslog().getFileDateFormat());
        valve.setRequestAttributesEnabled(tomcatProperties.getAccesslog().isRequestAttributesEnabled());
        valve.setRotatable(tomcatProperties.getAccesslog().isRotate());
        valve.setBuffered(tomcatProperties.getAccesslog().isBuffered());

        valve.setMaxDays(accessLogMaxDays);

        factory.addEngineValves(valve);
    }
}
